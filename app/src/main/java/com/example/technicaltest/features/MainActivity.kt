package com.example.technicaltest.features

import android.database.Observable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.technicaltest.R

import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.ViewModelLifecycle

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    val adapter by lazy {
        AlbumsAdapter(mutableListOf())
    }
    private lateinit var viewModel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        val recyclerView = findViewById<RecyclerView>(R.id.recycleview)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        bindData()
    }

    fun bindData() {
        viewModel.getAlbums()
        viewModel.allResults.observe(this, Observer {
            adapter.setItems(it)
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}