package com.example.technicaltest.features


import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope

import dagger.hilt.android.lifecycle.HiltViewModel

import com.example.technicaltest.database.dao.AlbumsEntity
import com.example.technicaltest.api.repository.AlbumsApiRepo
import com.example.technicaltest.database.repository.AlbumsDbRepo
import com.example.technicaltest.model.Albums
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val albumsApiRepo: AlbumsApiRepo,
    private val albumsDbRepo: AlbumsDbRepo
) : ViewModel() {


    val allResults = MutableLiveData<List<Albums>>()
    var job: Job? = null


    fun getAlbums() {
        job = viewModelScope.launch {

            Log.d("dddd", "sadsan")
            try {
                val response = albumsApiRepo.getAlbums()
                Log.d("dddd", response.isSuccessful.toString())
                if (response.isSuccessful) {
                    response.body()?.let {
                        withContext(Dispatchers.IO) {
                            val convertedAlbums = it.map { album ->
                                AlbumsEntity(
                                    id = album.id,
                                    albumId = album.albumId,
                                    title = album.title,
                                    url = album.url,
                                    thumbnailUrl = album.thumbnailUrl
                                )
                            }
                            albumsDbRepo.insertAlbums(convertedAlbums)
                        }
                        withContext(Dispatchers.Main) {
                            allResults.postValue(it.map { album ->
                                Albums(
                                    id = album.id,
                                    albumId = album.albumId,
                                    title = album.title,
                                    url = album.url,
                                    thumbnailUrl = album.thumbnailUrl
                                )
                            });

                        }
                    }
                } else {
                    withContext(Dispatchers.IO) {
                        val albums = albumsDbRepo.getAlbums()
                        allResults.postValue(albums.map { album ->
                            Albums(
                                id = album.id,
                                albumId = album.albumId,
                                title = album.title,
                                url = album.url,
                                thumbnailUrl = album.thumbnailUrl
                            )
                        });
                    }
                }

            } catch (e: IOException) {
                withContext(Dispatchers.IO) {
                    val albums = albumsDbRepo.getAlbums()
                    allResults.postValue(albums.map { album ->
                        Albums(
                            id = album.id,
                            albumId = album.albumId,
                            title = album.title,
                            url = album.url,
                            thumbnailUrl = album.thumbnailUrl
                        )
                    });
                }
            }
        }

    }


    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }

}