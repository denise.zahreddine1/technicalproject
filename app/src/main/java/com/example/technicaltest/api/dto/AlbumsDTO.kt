package com.example.technicaltest.api.dto

import com.google.gson.annotations.SerializedName

data class AlbumsDTO(
    @SerializedName("albumId") var albumId: Long,
    @SerializedName("id") var id: Long,
    @SerializedName("title") var title: String?,
    @SerializedName("url") var url: String?,
    @SerializedName("thumbnailUrl") var thumbnailUrl: String?,
)


