package com.example.technicaltest

import assertk.assertThat
import assertk.assertions.hasSize
import assertk.assertions.isEqualTo
import com.example.technicaltest.api.RestApi
import com.example.technicaltest.api.dto.AlbumsDTO
import com.example.technicaltest.api.repository.AlbumsApiRepo
import com.example.technicaltest.database.dao.AlbumsDao
import com.example.technicaltest.database.dao.AlbumsEntity
import com.example.technicaltest.database.repository.AlbumsDbRepo
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

class UnitTestDAO {

    private val albumsDao: AlbumsDao = mock()


    @Before
    fun setup() {
    }



    @Test
    fun retrieveAllAlbums() = runTest {
        // GIVEN
        whenever(albumsDao.getAll()).doReturn(MOCK_ALBUM_LIST)

        // WHEN
        val albumsDbRepo = AlbumsDbRepo(albumsDao)
        val result = albumsDbRepo.getAlbums()

        // THEN
        result.let {
            assertThat(it).hasSize(2)
        }

        verify(albumsDao, times(1)).getAll()
    }

    @Test
    fun insertAlbums() = runTest {
        // GIVEN
        whenever(albumsDao.insertAll(any())).doReturn(Unit)

        // WHEN
        val albumsDbRepo = AlbumsDbRepo(albumsDao)

        val result = albumsDbRepo.insertAlbums(MOCK_ALBUM_LIST)

        // THEN
        verify(albumsDao, times(1)).insertAll(any())
        assertThat(result).isEqualTo(Unit)
    }

    private val MOCK_ALBUM_LIST = listOf(
        AlbumsEntity(
            1L, 44, "title 1", "url 1", "url 2"
        ),
        AlbumsEntity(
            2L, 44, "title 2", "url 3", "url 4"
        ),
    )

}
