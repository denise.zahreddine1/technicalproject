package com.example.technicaltest.database

import android.content.Context
import androidx.room.Room
import com.example.technicaltest.database.dao.AlbumsDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

const val BASE_URL = "https://static.leboncoin.fr/"

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {


    @Provides
    @Singleton
    fun databaseInstance(@ApplicationContext applicationContext:Context):AppDatabase  {
        val db =  Room.databaseBuilder(
                applicationContext,
                AppDatabase::class.java, "albums-db"
            ).build()
        return db

    }

    @Provides
    fun provideAlbumsDao(appDatabase: AppDatabase): AlbumsDao {
        return appDatabase.albumsDao()
    }

}