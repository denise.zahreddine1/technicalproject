package com.example.technicaltest.model

data class Albums(
    var albumId: Long,
    var id: Long,
    var title: String?,
    var url: String?,
    var thumbnailUrl: String?,
)


