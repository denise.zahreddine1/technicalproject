package com.example.technicaltest.api


import com.example.technicaltest.api.dto.AlbumsDTO
import retrofit2.Response
import retrofit2.http.GET


interface RestApi {

    @GET("img/shared/technical-test.json")
    suspend fun getAlbums(
    ) : Response<List<AlbumsDTO>>

}