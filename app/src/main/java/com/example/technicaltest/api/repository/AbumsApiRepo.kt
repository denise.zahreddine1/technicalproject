package com.example.technicaltest.api.repository

import com.example.technicaltest.api.RestApi
import com.example.technicaltest.api.dto.AlbumsDTO

import retrofit2.Response
import javax.inject.Inject

class AlbumsApiRepo @Inject constructor(private val retrofitInstance: RestApi) {
    suspend fun getAlbums(): Response<List<AlbumsDTO>> {
        return retrofitInstance.getAlbums()
    }
}