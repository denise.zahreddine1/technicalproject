package com.example.technicaltest.features

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.technicaltest.R
import com.example.technicaltest.model.Albums


class AlbumsAdapter(
    private var items: List<Albums>
) : RecyclerView.Adapter<AlbumsAdapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var titleTxt: TextView

        var imageView: ImageView

        init {
            titleTxt = itemView.findViewById<TextView>(R.id.titleTxt)

            imageView = itemView.findViewById<ImageView>(R.id.imageView)
        }
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AlbumsAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val view: View = inflater.inflate(R.layout.item_album, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: AlbumsAdapter.ViewHolder, position: Int) {
        val item = items[position]

        holder.apply {
            titleTxt.text = item.title
            Glide.with(holder.itemView).load(item.thumbnailUrl).into(imageView);
        }
    }

    fun setItems(data: List<Albums>) {
        items = data
        this.notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return items.size
    }
}