package com.example.technicaltest.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface AlbumsDao {
    @Query("SELECT * FROM albumsentity")
    fun getAll(): List<AlbumsEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(albums: List<AlbumsEntity>)

}