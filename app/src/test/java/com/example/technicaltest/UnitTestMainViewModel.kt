package com.example.technicaltest

import android.util.Log
import assertk.assertThat
import assertk.assertions.hasSize
import assertk.assertions.isEqualTo
import com.example.technicaltest.api.dto.AlbumsDTO
import com.example.technicaltest.api.repository.AlbumsApiRepo
import com.example.technicaltest.database.dao.AlbumsDao
import com.example.technicaltest.database.dao.AlbumsEntity
import com.example.technicaltest.database.repository.AlbumsDbRepo
import com.example.technicaltest.features.MainViewModel
import com.example.technicaltest.model.Albums
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(MockitoJUnitRunner::class)
class UnitTestMainViewModel {

    private val albumsDbRepo: AlbumsDbRepo = mock()
    private val albumsApiRepo: AlbumsApiRepo = mock()


    @Before
    fun setup() {
    }


    @Test
    fun getAlbums() = runTest {
        // GIVEN
        whenever(albumsApiRepo.getAlbums()).doReturn(Response.success(MOCK_ALBUM_LIST))
        whenever(albumsDbRepo.getAlbums()).doReturn(MOCK_ALBUM_LIST_DB)

        // WHEN
        val mainViewModel = MainViewModel(albumsApiRepo, albumsDbRepo)
        launch {
            mainViewModel.getAlbums()
        }

        // THEN
        mainViewModel.allResults.value?.let {
            assertThat(it).hasSize(2)
        }

        verify(mainViewModel, times(1)).getAlbums()
    }


    private val MOCK_ALBUM_LIST = listOf(
        AlbumsDTO(
            1L, 44, "title 1", "url 1", "url 2"
        ),
        AlbumsDTO(
            2L, 44, "title 2", "url 3", "url 4"
        ),
    )
    private val MOCK_ALBUM_LIST_DB = listOf(
        AlbumsEntity(
            1L, 44, "title 1", "url 1", "url 2"
        ),
        AlbumsEntity(
            2L, 44, "title 2", "url 3", "url 4"
        ),
    )
}
