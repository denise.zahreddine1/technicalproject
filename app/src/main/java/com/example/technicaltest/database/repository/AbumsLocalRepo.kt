package com.example.technicaltest.database.repository

import com.example.technicaltest.database.dao.AlbumsEntity
import com.example.technicaltest.database.dao.AlbumsDao

import javax.inject.Inject



class AlbumsDbRepo @Inject constructor(private val dao: AlbumsDao) {
    suspend fun getAlbums(): List<AlbumsEntity>{
       return dao.getAll();
    }

    suspend fun insertAlbums( albums: List<AlbumsEntity>){
        return dao.insertAll(albums);
    }
}