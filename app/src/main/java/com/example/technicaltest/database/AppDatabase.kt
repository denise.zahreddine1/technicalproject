package com.example.technicaltest.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.technicaltest.database.dao.AlbumsEntity
import com.example.technicaltest.database.dao.AlbumsDao


@Database(entities = [AlbumsEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun albumsDao(): AlbumsDao
}